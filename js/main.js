// 1. Автоматизировать вопросы пользователю про фильмы при помощи цикла
// 2. Сделать так, чтобы пользователь не мог оставить ответ в виде пустой строки, отменить ответ или ввести название фильма длинее, чем 50 символов. Если это происходит - возвращаем пользователя к вопросам опять
// 3. При помощи условий проверить personalMovieDB.count, и если он меньше 10 - вывести сообщение 'Просмотрено довольно мало фильмов', если от 10 до 30 - 'Вы классический зритель', а если больше - 'Вы киноман'. А если не подошло ни к одному варианту - 'Произошла ошибка'
// 4. Потренироваться и переписать цикл еще двумя способами


//3

let numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?", "")

let personalMovieDB ={
	count: numberOfFilms,
	movies: {},
	actors: {},
	genres: [],
	private: false
}
if(personalMovieDB.count<10){
	alert("Просмотрено довольно мало фильмов")
}else if(personalMovieDB.count>=10 && personalMovieDB.count<=30){
	alert("Вы классический зритель")
}else if(personalMovieDB.count>30){
	alert("Вы киноман")
}else{
	alert("Произошла ошибка")
}

//1
let filmName;
let filmRating;
for(let i=0; i<2;i++){
filmName = prompt("Один из последних просмотренных фильмов?", "");
filmRating = prompt("На сколько оцените его?", "");

//2
if((filmName == "" || filmRating == "") || (filmName == null || filmRating == null) || (filmName.length > 50 || filmRating.length > 50)){
	i--
	continue
}
personalMovieDB.movies[filmName] = filmRating;
}

//4 
// еще 2 варианты цикла закомментированы


// let i = 0;
// while(i < 2){
// i++;
// filmName = prompt("Один из последних просмотренных фильмов?", "");
// filmRating = prompt("На сколько оцените его?", "");
// if((filmName == "" || filmRating == "") || (filmName == null || filmRating == null) || (filmName.length > 50 || filmRating.length > 50)){
// 	i--
// 	continue
// }
// personalMovieDB.movies[filmName] = filmRating;
// }


// let i = 0;
// do{
// 	i++;
// filmName = prompt("Один из последних просмотренных фильмов?", "");
// filmRating = prompt("На сколько оцените его?", "");
// if((filmName == "" || filmRating == "") || (filmName == null || filmRating == null) || (filmName.length > 50 || filmRating.length > 50)){
// 	console.log(i)
// 	console.log('зашел')
// 	i--
// 	console.log(i)
// 	continue
// }
// personalMovieDB.movies[filmName] = filmRating;
// }while(i < 2)
